##READ ME
Author: Brenden Watson
Contact: brendenw@uoregon.edu
This project uses Flask and Ajax to calculate and display brevet control points and times in a brevet race much like the calculator here (https://rusa.org/octime_acp.html). ACP standard is used for the opening and closing times of each control point. The calculator can be used on localhost. 
